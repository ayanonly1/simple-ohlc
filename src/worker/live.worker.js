/**
 * @todo: add reconnection logic
 * @todo: Move more chart rendering logic here so that only drawing stays in the main thread
 */

import {sub, unSub, onError, getData} from '../Components/Live/live.service.js';

import DtataStore from "../util/ohlc_binning";


const globalData = new DtataStore ();

globalData.setCallBack((binedData)=>{
    postMessage({type: 'data', payload: binedData});
})

onError((err)=>postMessage({tyep: 'error', payload: err}));

onmessage = function (event) {
    switch (event.data && event.data.type) {
        case "sub":
            sub();
            console.log('Sub');
        break;
        case "unSub":
            unSub();
            console.log('UnSub');
        break;
        case "viewConfig":
            if (event.payload) {
                globalData.setConfig(event.payload.width, event.payload.viewStart, event.payload.viewEnd);
            }
            console.log('viewConfig');
        break;
        default:
            throw new Error("Message not recognized");
    }
};


getData((data)=>{
    if (data && data.replace) {
        try {
            let point = JSON.parse("["+ data.replace(/\,$/ig, "") + "]");
            // ** Seems the data is not in correct format. Some time the High and low are not actual high and low.
            point[3] = Math.min(point[1], point[2], point[3], point[4]);
            point[2] = Math.max(point[1], point[2], point[3], point[4]);

            globalData.addData([point]);
            // send it to main thread.
            // postMessage({type: 'data', payload: globalData});
        } catch (e) {

        }
    }
})


