export default class Ohlc {
  constructor(ctx, data, config = {}) {
    this.config = {
        width: config.width,
        height: config.height,
      ctx,
      data,
      yAxis: {},
      xAxis: {
        zoom: 1,
        start: 0,
        pixPerPoint: 20,
      },
    };
    // @todo: make canvas dynamic
    let canvasConfig = config.canvas || {};
    this.config.canvas = {
        x: canvasConfig.x || 10,
        y: canvasConfig.y || 10,
        width: canvasConfig.width || 400,
        height: canvasConfig.height || 300,
      };
  }
  configX (config) {
    this.config.xAxis.zoom = config.zoom || this.config.xAxis.zoom
    this.config.xAxis.start = config.start === 0 ? config.start : (config.start || this.config.xAxis.start);
    this.draw();
  }
  calYAxis(data = []) {
    let yAxis = this.config.yAxis;
    data &&
      data.forEach((point) => {
        if (point) {
          yAxis.min = yAxis.min <= point[3] ? yAxis.min : point[3];
          yAxis.max = yAxis.max >= point[2] ? yAxis.max : point[2];
        }
      });

    yAxis.min = yAxis.min || 0;
    yAxis.max = yAxis.max === 0 || yAxis.max ? yAxis.max : 1;

    // ** in a wrong place
    yAxis.pixPerVal = this.config.canvas.height / (yAxis.max - yAxis.min);
  }
  getYPix(val) {
    return (
      this.config.canvas.y + ( this.config.canvas.height -
      (val - this.config.yAxis.min) * this.config.yAxis.pixPerVal
    ));
  }
  calXAxis (data = []) {
      if (data.length) {
        this.config.xAxis.pixPerPoint = (this.config.canvas.width / data.length) * this.config.xAxis.zoom;
      }
  }
  getXPix() {}
  drawAxis (ctx) {
    let canvas = this.config.canvas;
    // Draw the top and bottom axis border
    ctx.beginPath();
    ctx.strokeStyle = "#CCCCCC";
    //XAxis
    ctx.moveTo(canvas.x, this.getYPix(this.config.yAxis.max));
    ctx.lineTo(canvas.x, this.getYPix(this.config.yAxis.min));
    ctx.lineTo(canvas.x + canvas.width, this.getYPix(this.config.yAxis.min));
    ctx.stroke();
  }
  drawPlots(ctx, data) {
    let canvas = this.config.canvas,
      xAxis = this.config.xAxis,
      openCloseWidth = xAxis.pixPerPoint * 0.3;
    
    ctx.lineWidth = 1;

    let startIndex = Math.floor(this.config.xAxis.zoom > 1 ? (data.length * this.config.xAxis.start) : 0),
    endIndex = Math.ceil(startIndex + (data.length / this.config.xAxis.zoom)),
    x = canvas.x + xAxis.pixPerPoint / 2;


    for(let i = startIndex; i < endIndex; i ++) {
        let point = data[i];
        if (point) {
            let oY = this.getYPix(point[1]);
            let hY = this.getYPix(point[2]);
            let lY = this.getYPix(point[3]);
            let cY = this.getYPix(point[4]);

            ctx.strokeStyle = point[4] > point[1] ? "green" : "red";
            ctx.beginPath();
            // Vertical line
            ctx.moveTo(x, hY);
            ctx.lineTo(x, lY);
            // Open
            ctx.moveTo(x-openCloseWidth, oY);
            ctx.lineTo(x, oY);

            // close
            ctx.moveTo(x +openCloseWidth, cY);
            ctx.lineTo(x, cY);

            ctx.stroke();
        }

        // increment x (assumed there is no gap in data)
        x += this.config.xAxis.pixPerPoint;
    };

  }
  setData (data) {
    this.config.data = data;
  }
  draw(data) {
    if (data) {
        this.setData(data);
    }

    this.calYAxis(this.config.data);
    this.calXAxis(this.config.data);

    // Clear previous drawing
    this.config.ctx.clearRect(0, 0, this.config.width, this.config.height);

    this.drawPlots(this.config.ctx, this.config.data);
    this.drawAxis(this.config.ctx);
  }

//   redraw () {
//     this.calYAxis(this.config.data);
//     this.calXAxis(this.config.data);
//     this.drawPlots(this.config.ctx, this.config.data);
//   }
}
