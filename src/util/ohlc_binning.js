/**
 * @todo: Data is binned considering there is no null points in between. THis needs ot be calculated w.r.t. start and end time.
 * @todo: add stepped binning like, 1s, 1 min, 15 min, 1hr, 1d, 1w etc.
 * @todo: in case of zoomed view, don't bin data outside view
 */
let minPointWidth = 5,
maxPointWidth = 30;


export default class DtataStore {
    constructor () {
        this.data = [];
        this.config = {
            width: 200,
            viewStart: 0,
            viewEnd: 200
        };
    }
    addData (data) {
        if (data.length) {
            this.data = this.data.concat(data);
        }

        //@todo: Optimise so that we don't need to calculate complete bin always
        this.calculateBin();
        this.cb && this.cb(this.getCompleteData());
    }
    calculateBin () {
        if (this.data.length && this.config.width) {
            let perPointWidth = this.config.width / this.data.length;
            if (perPointWidth < minPointWidth) {
                this.config.bin = Math.ceil(minPointWidth / perPointWidth);
            } else {
                if (perPointWidth > maxPointWidth) {
                    this.config.nullCount = Math.ceil(this.config.width / maxPointWidth) - this.data.length;
                }
                this.config.bin = 1;
            }
        }
    }
    setConfig (width, viewStart, viewEnd) {
        this.config = {...this.config, width, viewStart, viewEnd}
    }
    getCompleteData () {
        let bin = this.config.bin;
        this.binnedData = this.data.reduce((acc, point, i )=>{
            if (i % bin) {
                let lastPoint = acc[acc.length - 1]
                // Start time and open will be the first one
                lastPoint[2] = lastPoint[2] > point[2] ? lastPoint[2] : point[2]
                lastPoint[3] = lastPoint[3] < point[3] ? lastPoint[3] : point[3]
                lastPoint[4] = point[4]; // close
                lastPoint[5] += point[5];
            } else {
                acc.push(point);
            }
            return acc;
        }, [])
        return this.binnedData;
    }
    setCallBack(cb) {
        this.cb = cb;
    }
}