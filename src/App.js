import { useState } from "react";
import "./App.css";
import Historical from "./Components/Historical/Historical";
import Live from "./Components/Live/Live";

function App() {
  const [tabIndex, setTabIndex] = useState(0);
  return (
    <div className="root">
      <div className="tabs">
        <div
          className={"tabs-inner-container" + (tabIndex === 0 ? " active" : " ")}
          onClick={() => setTabIndex(0)}
        >
          <div>Historical</div>
        </div>
        <div
          className={"tabs-inner-container" + (tabIndex === 1 ? " active" : " ")}
          onClick={() => setTabIndex(1)}
        >
          <div>Live</div>
        </div>
      </div>
      <div className="tabs_content-wrapper">
        {tabIndex ? <Live /> : <Historical />}
      </div>
    </div>
  );
}

export default App;
