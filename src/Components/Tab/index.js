function Tab() {
  return (
    <div class="dwc-tabs__tabsWrapper tab-type--horizontal">
      <ul class="tabs">
        <li class="tabs-inner-container">
          <div class="dwc-tabs__tab-header active" role="presentation">
            <div>Title 1</div>
          </div>
        </li>
        <li class="tabs-inner-container">
          <div class="dwc-tabs__tab-header" role="presentation">
            <div>Title 2</div>
          </div>
        </li>
        <li class="tabs-inner-container">
          <div class="dwc-tabs__tab-header" role="presentation">
            <div data-type="header">
              <div>Title 3</div>
            </div>
          </div>
        </li>
      </ul>
      <section class="dwc-tabs__content-wrapper">
        <div>This is the body of the Tab</div>
      </section>
    </div>
  );
}

export default Tab;
