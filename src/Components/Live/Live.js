/**
 * @todo: make the chart responsive
 * @todo: Add transition during zoom and zoom out etc.
 * @todo: Disable zoom pan buttons when not applicable
 */

import React, { Component, useEffect } from "react";
import Worker from "worker-loader!../../worker/live.worker.js"; // eslint-disable-line import/no-webpack-loader-syntax
import "./Live.css";
import Ohlc from "../../util/ohlc";

const worker = new Worker();

class Live extends Component {
  constructor(props) {
    super(props);

    //@todo: Make dynamic canvas w.r.t axis size
    this.config = {
        canvasTop: 30,
        canvasLeft: 70,
        canvasBottom: 50,
        canvasRight: 10,
        zoom: 1,
        start: 0,
        end: 1,
        isUpdating: true
    };

    // Add for message
    worker.addEventListener("message", (event) => {
      switch (event.data.type) {
        case "data":
          this.ohlc && this.ohlc.draw(event.data.payload);
          break;

        default:
          break;
      }
    });
    //subscriope for data
    worker.postMessage({ type: "sub" });
  }
  componentWillUnmount() {
    worker.postMessage({ type: "unSub" });
  }
  componentDidMount() {
    const canvas = document.getElementById("canvas");

    this.updateCanvasDim();

    this.ohlc = new Ohlc(canvas.getContext("2d"), [], {
        width: this.config.width,
        height: this.config.height,
      canvas: {
        x: this.config.canvasLeft,
        y: this.config.canvasTop,
        width: this.config.canvasWidth,
        height: this.config.canvasHeight,
      },
    });

    this.updateViewCOnfig();
  }
  toggleUpdate = () => {
    worker.postMessage({ type: this.config.isUpdating ? "unSub" : 'sub' });
    this.config.isUpdating = !this.config.isUpdating;
  }
  updateCanvasDim () {
    const canvas = document.getElementById("canvas");
    this.config.width = canvas.offsetWidth;
    this.config.height = canvas.offsetHeight;
    this.config.canvasWidth = this.config.width - this.config.canvasLeft - this.config.canvasRight;
    this.config.canvasHeight = this.config.height - this.config.canvasTop - this.config.canvasBottom;
  }
  updateViewCOnfig() {
    worker.postMessage({
      type: "viewConfig",
      payload: {
        width: this.config.canvasWidth * this.config.zoom,
        viewStart: this.config.start,
        viewEnd: this.config.end,
      },
    });
    this.ohlc.configX({
        zoom: this.config.zoom,
        start: this.config.start
    })
  }
  zoomIn = ()=>{
    let shift = (this.config.end - this.config.start) / 4;
    this.config.start = this.config.start + shift;
    this.config.end = this.config.end - shift;
    this.config.zoom = Math.round(1 / ((this.config.end - this.config.start)));
    console.log('***zoom in', this.config.zoom, this.config.start, this.config.end);
    this.updateViewCOnfig();
  }
  zoomOut = () => {
      if (this.config.zoom > 1) {
        let shift = (this.config.end - this.config.start) / 2;
        this.config.start = Math.max(this.config.start - shift, 0);
        this.config.end = Math.min(this.config.end + shift, 1);
        this.config.zoom = Math.round(1 / ((this.config.end - this.config.start)));
        console.log('***zoom out', this.config.zoom, this.config.start, this.config.end);
        this.updateViewCOnfig();
      }
  }
  panLeft = ()=>{
    if (this.config.start > 0) {
        let shift = this.config.start - Math.max(this.config.start - ((this.config.end - this.config.start) / 10), 0);
        this.config.start -= shift;
        this.config.end -= shift;
        // this.config.zoom = Math.round(1 / ((this.config.end - this.config.start)));
        console.log('***zoom out', this.config.zoom, this.config.start, this.config.end);
        this.updateViewCOnfig();
      }
  }
  panRight = ()=>{
    if (this.config.end < 1) {
        let shift =  Math.min(this.config.end + ((this.config.end - this.config.start) / 10), 1) - this.config.end;
        this.config.start += shift;
        this.config.end += shift;
        // this.config.zoom = Math.round(1 / ((this.config.end - this.config.start)));
        console.log('***zoom out', this.config.zoom, this.config.start, this.config.end);
        this.updateViewCOnfig();
      }
  }
  render() {
    return (
      <div className="chart-container">
        <div>
          <button type="button" onClick={this.zoomIn}>
            +
          </button>
          <button type="button" onClick={this.zoomOut}>
            -
          </button>
          <button type="button" onClick={this.panLeft}>
            &lt;
          </button>
          <button type="button" onClick={this.panRight}>
            &gt;
          </button>
          <button type="button" onClick={this.toggleUpdate}>
            Toggle live update
          </button>
        </div>
        <div>
          <canvas id="canvas" width="800" height="600"></canvas>
        </div>
      </div>
    );
  }
}

export default Live;
