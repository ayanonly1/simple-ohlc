import io from "socket.io-client";


var socket = io("http://kaboom.rksv.net/watch");
console.log(socket);

// var messages = document.getElementById("messages");
socket.on("connect", function (clientConnection) {
  console.log("client connection: ", clientConnection);
  //socket.emit("sub", { state: true });
});

// socket.on("data", function (msg, i) {
//   i(1);
// //   var item = document.createElement("li");
// //   item.textContent = msg;
// //   messages.appendChild(item);
// //   window.scrollTo(0, document.body.scrollHeight);
// });
// socket.on("error", function (err) {
//   console.log(err);
// });


// setTimeout(() => {
//     socket.emit('unsub', { state: false });
// }, 2000);


export default socket;

export const sub = () => {
    socket.emit("sub", { state: true });
};

export const unSub = () => {
    socket.emit("unsub", { state: false });
};

export const onError = (cb) => {
    socket.on("error", function (err) {
        cb(err);
      });
};

export const getData = (cb) => {
    socket.on("data", (msg, i) => {
        i(1);
        cb(msg);
    });
};
